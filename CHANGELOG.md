## [2.11.0] - 2022-08-16
### Added
- en_CA, en_GB translators

## [2.10.0] - 2022-08-16
### Added
- de_DE translators

## [2.9.0] - 2022-08-09
### Added
- en_AU translators

## [2.8.1] - 2022-07-04
### Fixed
- weight conversion from grams to grams

## [2.8.0] - 2022-06-07
### Added
- settings md5 hash

## [2.7.4] - 2021-11-02
### Added
- weight conversion to OZ

## [2.7.3] - 2021-01-12
### Fixed
- Polish translations
### Changed
- truncate time when date is blackout week day or lead day

## [2.7.0] - 2021-01-05
### Added
- Settings modifiers factories
- Blackout Lead Days

## [2.6.2] - 2020-07-01
### Added
- Shop price rounding precision in ShopSettings interface

## [2.6.1] - 2020-02-04
### Fixed
- CollectionPointNotFoundException in collection points methods

## [2.6.0] - 2020-02-04
### Added
- CanTestSettings interface

## [2.5.1] - 2020-01-15
### Changed
- units conversion

## [2.5.0] - 2020-01-15
### Added
- universal dimension inch conversion

## [2.4.0] - 2020-01-14
### Added
- item name

## [2.3.0] - 2019-12-27
### Added
- Estimated business days in transit

## [2.2.0] - 2019-12-18
### Added
- CanInsure capability
- CanPack capability
- CanReturnDeliveryDate capability
### Changed
- Modifiers namespace

## [2.1.0] - 2019-12-18
### Added
- SettingsDefinition modifiers

## [2.0.1] - 2019-12-16
### Added
- LBS weight unit support

## [2.0.0] - 2019-12-13
### Fixed
- added is_rate_enabled to CanRate interface

## [1.5.1] - 2019-12-10
### Fixed
- namespace in ShopSettings

## [1.5.0] - 2019-12-09
### Added
- packages
- shop settings interface

## [1.4.0] - 2019-12-03
### Added
- collection points

## [1.3.0] - 2019-12-03
### Added
- collection point zs

## [1.2.0] - 2019-11-27
### Added
- setting can have default value

## [1.1.0] - 2019-11-19
### Added
- item can have declared value

## [1.0.0] - 2019-11-13
### Added
- initial version
