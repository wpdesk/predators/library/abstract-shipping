<?php
/**
 * Capability: CanRate class
 *
 * @package WPDesk\AbstractShipping\Shipment
 */

namespace WPDesk\AbstractShipping\ShippingServiceCapability;

use WPDesk\AbstractShipping\Rate\ShipmentRating;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\AbstractShipping\Shipment\Shipment;

/**
 * Interface for rate shipment
 *
 * @package WPDesk\AbstractShipping\ShippingServiceCapability
 */
interface CanRate {

	/**
	 * Rate shipment.
	 *
	 * @param SettingsValues  $settings Settings.
	 * @param Shipment        $shipment Shipment.
	 *
	 * @return ShipmentRating
	 */
	public function rate_shipment(
		SettingsValues $settings,
		Shipment $shipment
	);

	/**
	 * Is rate enabled?
	 *
	 * @param SettingsValues $settings .
	 *
	 * @return bool
	 */
	public function is_rate_enabled( SettingsValues $settings );

}
