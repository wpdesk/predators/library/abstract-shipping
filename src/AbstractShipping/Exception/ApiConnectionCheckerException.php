<?php
/**
 * Exception thrown when connection checker fails.
 *
 * @package WPDesk\AbstractShipping\Exception
 */

namespace WPDesk\AbstractShipping\Exception;

class ApiConnectionCheckerException extends \RuntimeException {
}

