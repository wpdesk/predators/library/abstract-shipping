<?php
/**
 * Class BlackoutLeadDaysSettingsDefinitionDecoratorFactory
 *
 * @package WPDesk\AbstractShipping\Settings\SettingsDecorators
 */

namespace WPDesk\AbstractShipping\Settings\SettingsDecorators;

/**
 * Can create Blackout Lead Days settings decorator.
 */
class BlackoutLeadDaysSettingsDefinitionDecoratorFactory extends AbstractDecoratorFactory {

	const OPTION_ID = 'blackout_lead_days';

	/**
	 * @return string
	 */
	public function get_field_id() {
		return self::OPTION_ID;
	}

	/**
	 * @return array
	 */
	protected function get_field_settings() {
		return array(
			'title'             => __( 'Blackout Lead Days', 'abstract-shipping' ),
			'type'              => 'multiselect',
			'description'       => __(
				'Blackout Lead Days are used to define days of the week when shop is not processing orders.',
				'abstract-shipping'
			),
			'options'           => array(
				'1' => __( 'Monday', 'abstract-shipping' ),
				'2' => __( 'Tuesday', 'abstract-shipping' ),
				'3' => __( 'Wednesday', 'abstract-shipping' ),
				'4' => __( 'Thursday', 'abstract-shipping' ),
				'5' => __( 'Friday', 'abstract-shipping' ),
				'6' => __( 'Saturday', 'abstract-shipping' ),
				'7' => __( 'Sunday', 'abstract-shipping' ),
			),
			'custom_attributes' => array(
				'size' => 7,
			),
			'class'             => 'wc-enhanced-select',
			'desc_tip'          => true,
			'default'           => '',
		);
	}

}