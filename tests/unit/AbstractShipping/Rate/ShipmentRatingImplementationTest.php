<?php


use WPDesk\AbstractShipping\Rate\ShipmentRatingImplementation;
use WPDesk\AbstractShipping\Rate\SingleRate;

class ShipmentRatingImplementationTest extends PHPUnit\Framework\TestCase {

	public function test_get_ratings() {
		$array          = [ new SingleRate() ];
		$shipmentRating = new ShipmentRatingImplementation( $array );
		$this->assertInstanceOf( SingleRate::class, $shipmentRating->get_ratings()[0], 'Array has 1 single rate and it should be on index 0' );
	}
}
