<?php

use WPDesk\AbstractShipping\Shipment\Weight;
use WPDesk\AbstractShipping\UnitConversion\UniversalWeight;

class UniversalWeightTest extends PHPUnit\Framework\TestCase {

	public function test_can_convert_to_kg() {
		$weight = new UniversalWeight( '1000', Weight::WEIGHT_UNIT_G );
		$this->assertEquals( $weight->as_unit_rounded( Weight::WEIGHT_UNIT_KG ), 1, 'Converted G is not equal with KG' );
	}

	public function test_can_convert_to_lb() {
		$weight = new UniversalWeight( '453.59237', Weight::WEIGHT_UNIT_G );
		$this->assertEquals( $weight->as_unit_rounded( Weight::WEIGHT_UNIT_LB ), 1, 'Converted G is not equal with LB' );
	}

	public function test_can_convert_from_oz_to_kg() {
		$weight = new UniversalWeight( '35.28', Weight::WEIGHT_UNIT_OZ );
		$this->assertEquals( $weight->as_unit_rounded( Weight::WEIGHT_UNIT_KG ), 1, 'Converted OZ is not equal with KG' );
	}

}
