<?php

use WPDesk\AbstractShipping\Shipment\Dimensions;
use WPDesk\AbstractShipping\UnitConversion\UniversalDimension;

class UniversalDimensionTest extends PHPUnit\Framework\TestCase {

	public function test_can_convert_to_cm() {
		$dimenssion = new UniversalDimension( '10', Dimensions::DIMENSION_UNIT_MM );
		$this->assertEquals( $dimenssion->as_unit_rounded( Dimensions::DIMENSION_UNIT_CM ), 1, 'Converted CM is not equal with MM' );
	}

	public function test_can_convert_to_in() {
		$dimenssion = new UniversalDimension( '25.4', Dimensions::DIMENSION_UNIT_MM );
		$this->assertEquals( $dimenssion->as_unit_rounded( Dimensions::DIMENSION_UNIT_IN ), 1, 'Converted IN is not equal with MM' );
	}

	public function test_can_convert_from_cm() {
		$dimenssion = new UniversalDimension( '25.4', Dimensions::DIMENSION_UNIT_CM );
		$this->assertEquals( $dimenssion->as_unit_rounded( Dimensions::DIMENSION_UNIT_IN ), 10, 'Converted IN is not equal with CM' );
	}

}
