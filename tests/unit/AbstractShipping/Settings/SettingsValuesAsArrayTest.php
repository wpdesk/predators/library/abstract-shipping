<?php


use WPDesk\AbstractShipping\Settings\SettingsValuesAsArray;

class SettingsValuesAsArrayTest extends PHPUnit\Framework\TestCase {

	public function test_can_use_array() {
		$assoc_array = [ 'key' => 'value' ];
		$settings    = new SettingsValuesAsArray( $assoc_array );
		$this->assertTrue( $settings->has_value( 'key' ) );
		$this->assertEquals( 'value', $settings->get_value( 'key' ) );
	}

}
