<?php

use WPDesk\AbstractShipping\Settings\BlackoutLeadDays;

class BlackoutLeadDaysTest extends PHPUnit\Framework\TestCase {

	public function test_no_blackouted_days() {
		$date_monday = new DateTime( '2021-01-04' );
		$blackout_days = [];
		$blackout_lead_days = new BlackoutLeadDays( $blackout_days, 0 );

		$this->assertEquals( '2021-01-04', $blackout_lead_days->calculate_date( $date_monday )->format( 'Y-m-d' ) );
	}

	public function test_current_day_blackouted() {
		$date_monday = new DateTime( '2021-01-04' );
		$blackout_days = [ '1' ];
		$blackout_lead_days = new BlackoutLeadDays( $blackout_days, 0 );

		$this->assertEquals( '2021-01-05', $blackout_lead_days->calculate_date( $date_monday )->format( 'Y-m-d' ) );
	}

	public function test_tomorrow_blackouted_no_lead_days() {
		$date_monday = new DateTime( '2021-01-04' );
		$blackout_days = [ '2' ];
		$blackout_lead_days = new BlackoutLeadDays( $blackout_days, 0 );

		$this->assertEquals( '2021-01-04', $blackout_lead_days->calculate_date( $date_monday )->format( 'Y-m-d' ) );
	}

	public function test_tomorrow_blackouted_one_lead_day() {
		$date_monday = new DateTime( '2021-01-04' );
		$blackout_days = [ '2' ];
		$blackout_lead_days = new BlackoutLeadDays( $blackout_days, 1 );

		$this->assertEquals( '2021-01-06', $blackout_lead_days->calculate_date( $date_monday )->format( 'Y-m-d' ) );
	}

}
