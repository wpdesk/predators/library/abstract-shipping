<?php

use WPDesk\AbstractShipping\Exception\SettingsFieldNotExistsException;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierBefore;

class SettingsDefinitionModifierBeforeTest extends PHPUnit\Framework\TestCase {

	public function test_can_modify_settings() {
		$settings_definition = $this->createMock( SettingsDefinition::class );
		$settings_definition->method( 'get_form_fields' )->willReturn( array( 'first' => array(), 'last' => array() ) );
		$settings_definition_modifier = new SettingsDefinitionModifierBefore( $settings_definition, 'first', 'second', array( 'type' => 'type' ) );
		$this->assertEquals(
			array( 'first' => array(),  'second' => array( 'type' => 'type' ), 'last' => array() ),
			$settings_definition_modifier->get_form_fields()
		);
	}

	public function test_exception_when_field_not_found() {
		$this->expectException( SettingsFieldNotExistsException::class );
		$settings_definition = $this->createMock( SettingsDefinition::class );
		$settings_definition->method( 'get_form_fields' )->willReturn( array( 'last' => array() ) );
		$settings_definition_modifier = new SettingsDefinitionModifierBefore( $settings_definition, 'first', 'second', array( 'type' => 'type' ) );
		$settings_definition_modifier->get_form_fields();
	}

}
